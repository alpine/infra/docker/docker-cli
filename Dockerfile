FROM registry.alpinelinux.org/img/alpine:edge

ARG ARCH
RUN apk add --no-cache docker-cli docker-cli-buildx docker-cli-compose pigz

COPY scripts/ /usr/local/bin/
