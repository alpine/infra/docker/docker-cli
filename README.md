# docker-cli

Docker CLI tools based on Alpine.

## Helper scripts

### docker-push-image

Tags `<source_image>` as `<target_image>` and pushes it to the registry.

**Usage**:

    docker-push-image <source_image> <target_image>

### docker-push-manifest

Creates and pushes a docker manifest based on multiple source images.

**Usage**:

```
    docker-push-manifest <manifest> <image_prefix> <tag_suffix> <tag_suffix>..

    <manifest>          The name of the manifest to create and pushed
    <image_prefix>      The common prefix of each source image
    <tag_prefix>        One or more suffixes to that are used to specify the
                        source images
```
